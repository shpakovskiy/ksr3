<?php
$text = file_get_contents('text_windows1251.txt');//Считываем полный текст файла в переменную $text
//Выполняем задание (при разбивке по абзацам учтите, что в Windows1251 абзацы разделяются через "\r\n")





/*
 *
 * В папке ksr3 в файле text_windows1251.txt содержится текст и кодировке Windows-1251.
В этой же папке находится скрипт ksr3.php, в котором текст этого файла считывается в переменную $text.
Необходимо выполнить следующие задачи по обработке этого текста с использованием своих пользовательских функций (результат выводится в браузер в кодировке UTF-8):

1. Преобразовать текст в кодировку UTF-8.
2. Определить количество символов, количество слов и количество предложений для каждого абзаца.
3. Вывести первую букву каждого предложения жирным шрифтом.
4. Выделить цветом все слова HTML, PHP, ASP, ASP.NET, Java с любым регистром символов.

Постарайтесь при выполнении задания максимально использовать встроенные PHP-функции для обработки строк и массивов, а не писать собственные аналоги.
Задание непростое, так что обсуждение в нашей группе в вайбере не помешает
 *
 *
 */




//======================================
//1. Преобразовать текст в кодировку UTF-8.
//======================================

$text=mb_convert_encoding($text,'UTF-8','Windows-1251');

//======================================
//2. Определить количество символов, количество слов и количество предложений для каждого абзаца.
//======================================



//получение всех абзацев
$paragraph=explode("\r\n", $text);

//получение всех предложений
$sentence=explode(". ", $text);

function analyseText($arr){
    for($i=0;$i<count($arr);$i++){
        $symbol=mb_strlen($arr[$i]);
        $sentence=count(explode(". ", $arr[$i]));
        $word=count(explode(" ",$arr[$i]));


        echo "В абзаце номер $i: $sentence предложений, $word слов и $symbol символов. <br>";

    }
    return;
}


echo '<br>';
echo analyseText($paragraph);
echo '<br>';



//======================================
//3. Вывести первую букву каждого предложения жирным шрифтом.
//======================================


function makeBoldFirstChar($str){
    return "<span style='font-weight:bold'>".mb_substr($str,0,1)."</span>".mb_substr($str, 1);

}

function makeBoldFirstCharInAll($arr){
    $text=[];
    for($i=0;$i<count($arr);$i++){
        $text[]=makeBoldFirstChar($arr[$i]);
    }
    return implode(". ",$text);
}

echo '<br>';
echo makeBoldFirstCharInAll($sentence);
echo '<br>';




//======================================
//4. Выделить цветом все слова HTML, PHP, ASP, ASP.NET, Java с любым регистром символов.
//======================================

$arr = array("HTML", "PHP", "ASP", "Java");

function emphasizeWord(string $word,string $text){
    return str_replace($word, "<span style =\"color:red;\">$word</span>", $text);

}

function emphasizeText(array $arr,string $text){

    $word = implode("|", $arr);
    $text=emphasizeWord("ASP.NET", $text);
    return preg_replace("~($word)~i", "<span style =\"color:red;\">$1</span>", $text);


}

echo '<br>';
echo emphasizeText($arr, $text);
echo '<br>';






